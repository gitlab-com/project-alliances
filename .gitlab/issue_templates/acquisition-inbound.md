Please make sure you fill in the full details to the points written below:

1. Registered company name: 
1. Publicly available products and duration in state (months): 
    1. | Product name | Development/Market state | Duration |
       | ------------ | ------------------------ | -------- |
       |              |                          |          |
1. Founding team:
    1. | Name | Email | Phone number | Location |
       | ---- | ----- | ------------ | -------- |
       |      |       |              |          |
       |      |       |              |          |
1. Current headcount broken by departments:
    1. | Department   | Employees |
       | ------------ | --------- |
       | Engineering  |           |
       | Product      |           |
       | Sales        |           |
       | Support      |           |
       | Admin        |           |
       | ...          |           |
1. VC funding:
    1. | Round | Funding |
       | ----- | ------- |
       |       |         |
1. Staff locations:
1. State of developed products and duration in state (example: beta/GA for X months):
1. Current ARR:
1. Number of active clients:
    1. SMB:
    1. Enterprise:

/cc @bjung @eliran.mesika
/label acquisitions-inbound
/confidential
