This is an issue template for a first engagement on a joint integration of GitLab 
with a third party platform/service/product. Use this to log the context and team
involved and create a common ground to manage the collaboration.  Critical for us to identify the long-term support/engineering requirements

## Objective of the partnership:
GitLab: Be easiest DevOps tool to deploy/maintain on xxx (VMware, K8s, Openstack, bare metal).  We want to be the “Hello World” example for our partner.  GitLab ships with every demo/test environment.  
  
Partner: 

  
## Team
1. GitLab Leads: 
    1. Alliances:
    1. Partner/Product Marketing: 
    1. Product Management:
    1. Engineering:
1. [THIRD PARTY]:
    1. Alliance/Relationship Lead:
    2. Marketing:
    3. Product Management:
    4. Engineering:

### Goals
Describe the goals of this integration in an iterative way and what each stage 
would help achieve. Aim for a MVC for the first iteration. 

| Iteration | Goal | Timeline |
| --------- | ---- | -------- |
|           |      |          |
|           |      |          |

## Integrations/Tooling Requirements
### Code/Integration being used
Identify the standard code/tools that would be used.  Example would be our standard HELM chart or Ominbus package. 

### Known gaps
List any known gaps/blockers preventing the integration:
1.

### Ongoing maintenance
Any new integration has to be reviewed for continued support and maintenance.
[ ] Future maintenance review by Distribution PM

### References/Documents
1. 

## Related marketing activities
Ideas for joint marketing effort (e.g. announcement, follow up campaigns etc.)
1. 